<?php

namespace Project\Import\Utility\Westtech;

use Exception,
    CIBlockElement,
    CIBlockSection,
    Project\Import\Config;

class Catalog {

    static public function getSectionId($sectionId, $parent = 0) {
        static $sections = array();
        if (empty($sections[$parent])) {
            $arFilter = Array(
                'IBLOCK_ID' => Config::IBLOCK_ID,
                'SECTION_ID' => 0
            );
            if ($parent) {
                $arFilter['UF_OLD_ID'] = $parent;
                $res = CIBlockSection::GetList(array(), $arFilter, false, array('ID'));
                if ($arItem = $res->Fetch()) {
                    $arFilter['SECTION_ID'] = $arItem['ID'];
                    unset($arFilter['UF_OLD_ID']);
                } else {
                    return false;
                }
            }
            $res = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'UF_OLD_ID'));
            while ($arItem = $res->Fetch()) {
                $sections[$parent][$arItem['UF_OLD_ID']] = $arItem['ID'];
            }
        }
        if (empty($sections[$parent][$sectionId])) {
            pre($sections[$parent], $parent, $sectionId);
            throw new Exception('не найден раздел' . $sectionId);
        }
        return $sections[$parent][$sectionId];
    }

    static public function importSection($arItem, $categ, $section) {
        $productId = $arItem['ID'];
        $res = CIBlockElement::GetElementGroups($productId, true);
        $arGroups = array();
        while ($arGroup = $res->Fetch()) {
            $arGroups[$arGroup["ID"]] = $arGroup["ID"];
        }
//        pre($productId, $categ, $section, $arGroups);
        $isUpdate = false;
        if ($section) {
            $sectionID = self::getSectionId($section, $categ);
        } elseif ($categ) {
            $sectionID = self::getSectionId($categ);
        } else {
            return;
        }
        if ($sectionID and empty($arGroups[$sectionID])) {
            $isUpdate = true;
            $arGroups[$sectionID] = $sectionID;
        }
//        pre($arGroups);
//        exit;
        if ($isUpdate and $arGroups) {
//            preExit($arItem, $sections, $arGroups);
//            pre($productId, $sections, $arGroups);
            CIBlockElement::SetElementSection($productId, $arGroups);
//            true;
        }
        return false;
    }

}
