<?php

namespace Project\Import\Parse\Westtech;

use Exception,
    CIBlockElement,
    CIBlockSection,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Traits,
    Project\Import\Utility\Westtech\Catalog;

class Section {

    const LIMIT = 100;

    use Traits\Westtech\Catalog;

    public static function importProduct($arData) {
        if (empty($arData['alias'])) {
            return;
        }
//        pre($arData);

        $categ = $arData['categ'];
        $section = $arData['subcat'];

        $arFilter = Array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
            'PROPERTY_OLD_ID' => $arData['id'],
//            'ACTIVE' => 'Y',
//            'SECTION_ID' => 2375,
        );
        $arSelect = Array(
            'ID',
            'NAME',
        );
//        pre($arFilter);
        $res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        $is = false;
        while ($arItem = $res->fetch()) {
//            pre($arItem);
            $is = true;
            if(Catalog::importSection($arItem, $categ, $section)) {
                Logs::success('Обновлены товары', $arItem['NAME']);
            }
        }
//        if (empty($is)) {
//            preExit($arData);
//            exit;
//        }
//        exit;
    }

}
