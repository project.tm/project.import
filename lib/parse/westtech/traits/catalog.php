<?php

namespace Project\Import\Traits\Westtech;

use CDBResult,
    Project\Import\Search\Westtech\ItemTable,
    Bitrix\Main\Application,
    Project\Import\View;

trait Catalog {

    static public function getFieldsCatalog() {
        return array(
            'id',
            'id',
            'item',
            'alias',
            'categ',
            'subcat'
        );
    }

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        Application::getConnection('old')->queryExecute("SET NAMES 'cp1251'");
        Application::getConnection('old')->queryExecute('SET collation_connection = "cp1251_general_ci"');

        $param = array(
            'select' => array(new \Bitrix\Main\Entity\ExpressionField('FOUND_ROWS', 'SQL_CALC_FOUND_ROWS %s', 'id')) + static::getFieldsCatalog(),
            'filter' => array(
//                'subcat' => 12,
//                'item' => 'Behringer CL208',
                '!alias' => false,
                'enable' => array(1),
            ),
            'limit' => $limit,
            'offset' => $start
        );
        $rsData = ItemTable::GetList($param);
        $rsData = new CDBResult($rsData);

        $count = Application::getConnection('old')->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            self::importProduct($arItem);
        }

//        exit;
        return $pageIsNext;
    }

}
