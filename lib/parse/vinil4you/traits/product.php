<?php

namespace Project\Import\Traits\Vinil4you;

use CIBlockElement,
    Project\Import\Utility\Vinil4you\Content,
    Project\Log,
    Project\Import\Data,
    Project\Import\Config;

trait Product {

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $arFilter = array(
            "IBLOCK_ID" => Config::IBLOCK_ID,
        );
        $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, Array("nPageSize" => $limit, "iNumPage" => $page), array("ID", 'IBLOCK_ID', 'PROPERTY_OLD_ID', 'PROPERTY_SALELEADER', 'PROPERTY_RECOMMEND'));
        $count = $res->SelectedRowsCount();
        $pageCount = $res->NavPageCount;
        $pageItem = $res->NavPageNomer;
        $pageIsNext = $pageItem < $pageCount;

        $arUser = array(
            'act' => 'login',
            'action_to' => 'http://storeland.ru/',
            'site_id' => '',
            'to' => '',
            'form' => array(
                'user_mail' => static::LOGIN,
                'user_pass' => static::PASSWORD,
            ),
        );

        echo '<h3>Выполнено ' . round(($page - 1) / ceil($count / $limit) * 100, 5) . '% (' . ($page - 1) * $limit . '/' . $count . ')</h3>';
        while ($arItem = $res->Fetch()) {
//            pre($arItem['PROPERTY_OLD_ID_VALUE']);
            if (empty($arItem['PROPERTY_OLD_ID_VALUE'])) {
//                preExit($arItem);
                continue;
            }
            $data = Content::get($arItem['PROPERTY_OLD_ID_VALUE'], $arUser);
//            $data = Content::get($arItem['PROPERTY_OLD_ID_VALUE']=20993063, $arUser);

            preg_match('~' . preg_quote('<input type="hidden" name="form[goods_cat_id]" value="') . '([^"]*)' . preg_quote('"') . '~imsU', $data, $tmp);
            $section = array_map(function($data) {
                return str_replace('cid_', '', $data);
            }, explode(',', trim($tmp[1])));
            foreach ($section as $key => $value) {
                if (empty($value)) {
                    unset($section[$key]);
                }
            }
            self::importSection($arItem, $section);


            preg_match_all('~"related_goods_galery_([^"]+)"~imsU', $data, $tmp);
            if ($tmp[1]) {
                $key = array_combine($tmp[1], $tmp[1]);
//                pre($arItem);
                self::importRecommend($arItem, $key);
            }
//            if ($tmp[1]) {
//                preExit($arItem);
//            }
//            pre($key);
//            preHtml($data);
//            exit;
//            exit;
        }
//        exit;
        if ($pageIsNext) {
            return true;
        }
        return false;
    }

}
