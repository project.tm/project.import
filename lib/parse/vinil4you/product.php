<?php

namespace Project\Import\Parse\Vinil4you;

use Exception,
    CIBlockElement,
    CIBlockSection,
    Project\Import\Settings,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Traits;

class Product {

    const LIMIT = 30;
    const LOGIN = '*';
    const PASSWORD = '*';

    use Traits\Vinil4you\Product;

    static public function getSectionId($sectionId) {
        static $sections = array();
        if (empty($sections)) {
            $arFilter = Array('IBLOCK_ID' => Config::IBLOCK_ID);
            $res = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'UF_ID'));
            while ($arItem = $res->Fetch()) {
                $sections[$arItem['UF_ID']] = $arItem['ID'];
            }
        }
        if (empty($sections[$sectionId])) {
            throw new Exception('не найден раздел' . $sectionId);
        }
        return $sections[$sectionId];
    }

    static public function importRecommend($arItem, $key) {
        if($key and empty($arItem['PROPERTY_RECOMMEND_VALUE'])) {
            $arFilter = array(
                "IBLOCK_ID" => Config::IBLOCK_ID,
                "PROPERTY_OLD_ID" => $key,
            );
            $res = CIBlockElement::GetList(array("ID" => "ASC"), $arFilter, false, false, array("ID", 'IBLOCK_ID'));
            $recommend = array();
            while ($arElement = $res->Fetch()) {
                $recommend[] = $arElement['ID'];
            }
            CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], $recommend, 'RECOMMEND');
        }
    }

    static public function importSection($arItem, $sections) {
        $productId = $arItem['ID'];
        $res = CIBlockElement::GetElementGroups($productId, true);
        $arGroups = array();
        while ($arGroup = $res->Fetch()) {
            $arGroups[$arGroup["ID"]] = $arGroup["ID"];
        }
//        pre($productId, $sections, $arGroups);
        $isUpdate = false;
        foreach ($sections as $key => $value) {
            if (is_numeric($value)) {
                $value = self::getSectionId($value);
                if (empty($arGroups[$value])) {
                    $arGroups[$value] = $value;
                    $isUpdate = true;
                }
            } else {
                if ($value == 'favorites' and empty($arItem['PROPERTY_SALELEADER_VALUE'])) {
                    CIBlockElement::SetPropertyValues($arItem['ID'], $arItem['IBLOCK_ID'], 2, 'SALELEADER');
                }
            }
        }

        if ($isUpdate) {
//      preExit($arItem, $sections);
//            pre($productId, $sections, $arGroups);
            CIBlockElement::SetElementSection($productId, $arGroups);
//            exit;
        }

        Logs::success('Обновлены товары', $productId);
    }

}
