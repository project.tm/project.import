<?php

namespace Project\Import\Parse\Vinil4you;

use cFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    CCatalogProduct,
    CCatalogSKU,
    CPrice,
    Cutil,
    Project\Core\Redirect,
    Project\Import\Utility\Catalog,
    Project\Import\Settings,
    Project\Import\Data,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Traits;

class Yandex {

    use Traits\Yandex;

    static private $sections = array();

    static public function getSectionId($ID) {
        return self::$sections[$ID] ?: '';
    }

    static public function importSections($sections) {
        $bs = new CIBlockSection;
        foreach ($sections->children() as $data) {
            $arFilter = Array('IBLOCK_ID' => Config::IBLOCK_ID, 'NAME' => (string) $data);
            $res = CIBlockSection::GetList(array(), $arFilter, false, array('ID', 'NAME', 'UF_ID'));
            if ($arItem = $res->Fetch()) {
                $id = (int) $data->attributes()->id;
                if ($arItem['UF_ID'] != $id) {
                    $bs->Update($arItem['ID'], array(
                        'UF_ID' => $id
                    ));
                }
                self::$sections[$id] = $arItem['ID'];
            } else {
                pre($arFilter);
            }
        }
    }

    static public function importProduct($offers) {
//        pre($offers);
        $isOffers = true;

        $id = (int) $offers->attributes()->id;
        $articul = trim($offers->vendorCode);
        $nameOffers = trim($offers->name);
        $name = trim(substr($nameOffers, 0, strpos($nameOffers, '(')));
        if (empty($name)) {
            $name = $nameOffers;
        }
//        pre($name, $nameOffers);

        $url = $urlOffers = trim($offers->url);
        if (strpos($url, '?')) {
            $url = trim(substr($url, 0, strpos($url, '?') ?: false));
        }
        $uri = str_replace('http://vinyl4you.ru', '', $url);
        $uriOffers = str_replace('http://vinyl4you.ru', '', $urlOffers);
        $url = str_replace('http://vinyl4you.ru/goods/', '', $url);
//        pre($url, $urlOffers);
//        pre($uri, $uriOffers);

        $price = (int) str_replace(' ', '', $offers->price);
        $priceId = (string) $offers->currencyId;
//        pre($price, $priceId);

        $img = (string) $offers->picture;
//        pre($img);
        $arParams = array();
        foreach ($offers->param as $value) {
            $pName = (string) $value->attributes()->name;
            $pName = trim(str_replace(':', '', $pName));
            if (in_array($pName, array(
                        'Название х-ки товара №1',
                        'Название х-ки товара №2',
                    ))) {
                continue;
            }
            $arParams[$pName] = (string) $value;
        }


        $code = strtolower($url);
        $arFields = array(
            'IBLOCK_ID' => Config::IBLOCK_ID,
            'IBLOCK_SECTION_ID' => self::getSectionId((int) $offers->categoryId),
            'NAME' => $name,
            'SORT' => '500',
            'CODE' => strtolower($code),
        );
        $propFields = array(
            'OLD_ID' => $id,
            'ARTNUMBER' => $articul,
//            'ROLLWEIGHT' => Catalog::searchHl('Rollweight', $id, $value),
//            'SEARCH' => preg_replace('~([^a-z0-9Рђ-РЇ])~is', '', $articul)
        );
        Catalog::searchHl($propFields, $arParams, 'Airducts', 'Воздушные каналы');
        Catalog::searchHl($propFields, $arParams, 'Material', 'Материал');
        Catalog::searchHl($propFields, $arParams, 'Country', 'Страна производитель');
        if (!$isOffers) {
            Catalog::searchHl($propFields, $arParams, 'Modification', 'Модификация');
            Catalog::searchHl($propFields, $arParams, 'Numberofcapes', 'Количество накидок');
            Catalog::searchHl($propFields, $arParams, 'Throughput', 'Пропускаемость');
            Catalog::searchHl($propFields, $arParams, 'Plinth', 'Цоколь');
            Catalog::searchHl($propFields, $arParams, 'Temperature', 'Температура');
            Catalog::searchHl($propFields, $arParams, 'Colorcovers', 'Цвет чехлов');
            Catalog::searchHl($propFields, $arParams, 'Color', 'Цвет');
            Catalog::searchHl($propFields, $arParams, 'Rollweight', 'Вес рулона');
            Catalog::searchHl($propFields, $arParams, 'Size', 'Размер');
            Catalog::searchHl($propFields, $arParams, 'Measure', 'Измерять');
            Catalog::searchHl($propFields, $arParams, 'Filmwidth', 'Ширина пленки');
            if (!empty($arParams)) {
                pre($urlOffers);
                preExit($arParams, $propFields);
            }
        }

//        pre($arFields, $propFields);
        $arItem = Catalog::searchProductByCode($arFields, $propFields);
        if (empty($arItem)) {
            Logs::error('Не найдены товары', $name);
            return;
        }
        Redirect::add($uri, 'PRODUCT', $arItem['ID']);
//        if ('Черная алмазная крошка' != $arFields['NAME']) {
//            return;
//        }
        $arFields = array();
        if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
            if ($arFile = Catalog::uploadImage($img, true)) {
                $arFields["DETAIL_PICTURE"] = $arFile;
            }
//            if(empty($arFields["DETAIL_PICTURE"])) {
//                preExit($arItem, $img);
//            }
        }
        Catalog::updateProduct($arItem, $arFields, $propFields);
        Catalog::saveCatalog($arItem, $isOffers);

        if (empty($isOffers)) {
            Catalog::savePrice($arItem, $price, $priceId);
        }
//        if($articul=='Gazyeco2blbl') {
//            pre($img);
//            preExit($arItem, $img);
//        }

        if ($isOffers) {
            $productId = $arItem['ID'];
//            pre($arItem['ID']);
            $arFields = array(
                'IBLOCK_ID' => Config::OFFERS_ID,
                'NAME' => $name,
                'CODE' => $articul,
            );
            $propFields = array(
                'OLD_ID' => $id,
                'ARTNUMBER' => $articul,
                'CML2_LINK' => $productId
            );

//            pre($arFields, $propFields);
//            $arItem = Catalog::searchProductByCode($arFields, $propFields);
            $arItem = Catalog::searchProductByOldId($arFields, $propFields);
            Redirect::add($uriOffers, 'PRODUCT', $productId, $arItem['ID']);
            $arFields = array(
                'NAME' => $name,
            );
            Catalog::searchHl($propFields, $arParams, 'Modification', 'Модификация');
            Catalog::searchHl($propFields, $arParams, 'Numberofcapes', 'Количество накидок');
            Catalog::searchHl($propFields, $arParams, 'Throughput', 'Пропускаемость');
            Catalog::searchHl($propFields, $arParams, 'Plinth', 'Цоколь');
            Catalog::searchHl($propFields, $arParams, 'Temperature', 'Температура');
            Catalog::searchHl($propFields, $arParams, 'Colorcovers', 'Цвет чехлов');
            Catalog::searchHl($propFields, $arParams, 'Color', 'Цвет');
            Catalog::searchHl($propFields, $arParams, 'Rollweight', 'Вес рулона');
            Catalog::searchHl($propFields, $arParams, 'Size', 'Размер');
            Catalog::searchHl($propFields, $arParams, 'Measure', 'Измерять');
            Catalog::searchHl($propFields, $arParams, 'Filmwidth', 'Ширина пленки');
            if (!empty($arParams)) {
                pre($urlOffers);
                preExit($arParams, $propFields);
            }
            if(count($propFields)<3) {
                $arParams = array(
                    'Модификация'=>'Базовая'
                );
                Catalog::searchHl($propFields, $arParams, 'Modification', 'Модификация');
            }
//            pre($arParams, $propFields);
            Catalog::updateProduct($arItem, $arFields, $propFields);
            Catalog::saveCatalog($arItem, $isOffers);
            Catalog::savePrice($arItem, $price, $priceId);

//            preExit($arItem);
            
        }
//
//
//        exit;
//        if($productId==17085) {
//            exit;
//        }
////        preExit($arItem);

        Logs::success('Обновлены товары', $name);
    }

}
