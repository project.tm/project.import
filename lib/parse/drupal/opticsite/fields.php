<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\View,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Fields {

    const LIMIT = 100;
    const TYPE = array(
        'field_status' => 'StoreStatus'
    );

    static public function process($page) {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $limit = static::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        $connect->queryExecute("SET NAMES 'utf8'");
        $connect->queryExecute('SET collation_connection = "utf8_general_ci"');

        $rsData = $connect->query('SELECT SQL_CALC_FOUND_ROWS f.* FROM content_node_field f WHERE f.field_name IN("' . implode('", "', array_keys(self::TYPE)) . '") LIMIT ' . $start . ', ' . $limit);

        $count = $connect->queryScalar('SELECT FOUND_ROWS() as TOTAL');
        $pageIsNext = ($limit * $page) < $count;
        View::processed($page, $limit, $count);
        while ($arItem = $rsData->Fetch()) {
            self::importItem($arItem);
        }
        return $pageIsNext;
    }

    public static function importItem($arData) {
        static $arClass;
        $tName = self::TYPE[$arData['field_name']];
        if (empty($arClass[$name])) {
            $arClass[$tName] = \Project\Core\Highload::get($tName);
        }

        if ($arData['global_settings']) {
            try {
                $arData['global_settings'] = unserialize($arData['global_settings']);
                if ($arData['global_settings']['allowed_values']) {
                    foreach (array_map('trim', explode(PHP_EOL, $arData['global_settings']['allowed_values'])) as $value) {
                        $arFields = array();
                        list($arFields['UF_XML_ID'], $arFields['UF_NAME']) = array_map('trim', explode('|', $value));
                        $arFields['UF_OLD_ID'][] = $arFields['UF_XML_ID'];

                        $rsData = $arClass[$tName]::getList(array(
                                    'select' => array('ID', 'UF_XML_ID', 'UF_NAME', 'UF_FILE', 'UF_OLD_ID', 'UF_FULL_DESCRIPTION'),
                                    'filter' => array(
                                        'LOGIC' => 'OR',
                                        array('=UF_NAME' => $arFields['UF_NAME']),
                                        array('=UF_OLD_ID' => $arFields['UF_OLD_ID']),
                                    )
                        ));
                        $rsData = new CDBResult($rsData);
                        if ($arItem = $rsData->Fetch()) {
                            if (!in_array($arData['UF_XML_ID'], $arItem["UF_OLD_ID"])) {
                                $arFields["UF_OLD_ID"] = $arItem["UF_OLD_ID"];
                                $arFields["UF_OLD_ID"][] = $arData['UF_XML_ID'];
                            }
                            $arClass[$tName]::update($arItem['ID'], $arFields);
                        } else {
                            $arFields["UF_OLD_ID"] = array(
                                $arData['UF_XML_ID']
                            );
                            $arClass[$tName]::add($arFields);
                        }
                    }
                }
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
            }
        }

//        preExit($arData);
//        exit;


        Log::success('Обновлен: ' . $arData['tName'], $arFields['UF_NAME']);
    }

}
