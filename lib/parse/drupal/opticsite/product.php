<?php

namespace Project\Import\Parse\Drupal\Opticsite;

use Exception,
    Cutil,
    CFile,
    CDBResult,
    CIBlockElement,
    CIBlockSection,
    Bitrix\Main\Application,
    Project\Redirect\Redirect,
    Project\Import\Config,
    Project\Import\Log,
    Project\Import\Utility;

class Product {

    const LIMIT = 100;
    const TYPE = array(
        /*
         * услуги
         */
//        'making_optic' => 6,
//        'services_oth' => 6,
        /*
         * аксессуары
         */
//        'access_glasses' => 5,
//        /*
//         * Очки
//         */
//        'sport_glasses' => 4,
//        'sun_glasses' => 3,
//            /*
//             * Линзы очковые
//             */
        'och_linzu' => 2,
//        /*
//         * Оправы
//         */
//        'glasses' => 1,
    );

    static protected function getType() {
        return array_keys(self::TYPE);
    }

    use Traits\Node;

    public static function importProduct($arData) {
        $connect = Application::getConnection(Model\NodeTable::getConnectionName());
        if (empty($arData['url']['dst'])) {
//            preExit($arData);
            return;
        }

        $arData['product'] = $connect->query('SELECT u.* FROM uc_products u  WHERE u.nid="' . (int) $arData['nid'] . '" AND u.vid="' . (int) $arData['vid'] . '"')
                ->fetch();
        $arData['body'] = Utility\Content::uploadSrcImage($arData['body'], 'http://opticsite.ru');
        $arData['meta'] = Utility\Content::getMeta('http://opticsite.ru/node/' . (int) $arData['nid']);

        $arFields = array(
            'DATE_ACTIVE_FROM' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'TIMESTAMP_X' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'DATE_CREATE' => ConvertTimeStamp($arData['changed'], 'FULL'),
            'IBLOCK_ID' => Config::CATALOG_ID,
            'IBLOCK_SECTION_ID' => self::TYPE[$arData['type']],
            'NAME' => $arData['title'],
            'SORT' => '500',
            'ACTIVE' => $arData['status'] ? 'Y' : 'N',
            'CODE' => Cutil::translit(empty($arData['url']['dst']) ? $arData['title'] : str_replace(array('service/'), '', $arData['url']['dst']), "ru", array("replace_space" => "-", "replace_other" => "-")),
            'DETAIL_TEXT' => $arData['body'],
            'DETAIL_TEXT_TYPE' => 'html',
            'PREVIEW_TEXT' => $arData['teaser'],
            'PREVIEW_TEXT_TYPE' => 'html',
        );
        $propFields = array(
            'TEST' => 56,
            'MODEL' => $arData['product']['model'] ?: '',
            'OLD_NID' => $arData['nid'],
            'OLD_VID' => $arData['vid'],
            'TITLE' => $arData['meta']['title'] ?: '',
            'KEYWORDS' => $arData['meta']['keywords'] ?: '',
            'META_DESCRIPTION' => $arData['meta']['description'] ?: '',
        );
//        pre($arData, $arFields, $propFields);

        $arItem = Utility\Catalog::searchByName($arFields, $propFields);
        if (empty($arItem)) {
            Log::error('Не найдены товары', $name);
            return;
        }
        if ($arData['url']['dst']) {
            Redirect::add('/' . $arData['url']['dst'], 'CATALOG', $arItem['ID']);
//            pre('/' . $arData['url']['dst'], 'CATALOG', $arItem['ID']);
        }
        Redirect::add('/node/' . $arData['nid'], 'CATALOG', $arItem['ID']);
//        pre('/node/' . $arData['nid'], 'CATALOG', $arItem['ID']);
        $arFields = array();
        if ($arData['img']) {
            $img = array_shift($arData['img']);
            if (empty($arItem['DETAIL_PICTURE']) and ! empty($img)) {
                if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                    $arFields["DETAIL_PICTURE"] = $arFile;
                }
            }
            if (empty($arItem['PROPERTY_MORE_PHOTO_VALUE']) and ! empty($img)) {
                foreach ($arData['img'] as $img) {
                    if ($arFile = Utility\Image::upload($img, 'http://opticsite.ru/')) {
                        $propFields["MORE_PHOTO"][] = $arFile;
                    }
                }
            }
        }

        $param = array(
            'STORE_STATUS' => 'status',
            'SEX' => 'sex',
            'BRAND' => 'brand',
            'CYL_MIN' => 'formcyl_start',
            'CYL_MAX' => 'formcyl_end',
            'ADD_MIN' => 'formadd_start',
            'ADD_MAX' => 'formadd_end',
            'SPH_MIN' => 'formsph_start',
            'SPH_MAX' => 'formsph_end',
            'SPH_STEP' => 'formsph_step',
            'D' => 'formd',
            'light_l' => 'index',
//            'IMAGE' => array(
//                'image' => 'fid'
//            ),
        );
        $arParam = array();
        foreach ($param as $key => $value) {
            if (is_array($value)) {
                list($value, $field) = each($value);
            } else {
                $field = 'value';
            }
            $res = $connect->query('SELECT field_' . $value . '_' . $field . ' AS value FROM content_field_' . $value . '  WHERE nid="' . (int) $arData['nid'] . '" AND vid="' . (int) $arData['vid'] . '"');
            while ($item = $res->Fetch()) {
                if ($item['value']) {
                    $arParam[$key][] = Utility\Content::parseFloat($item);
                }
            }
        }
        $res = $connect->query('SELECT n.tid, t.name, v.name AS tName, h.parent FROM term_node n LEFT JOIN term_data t ON (t.tid=n.tid) LEFT JOIN vocabulary v ON(t.vid=v.vid) LEFT JOIN term_hierarchy h ON(h.tid=t.tid) WHERE n.nid="' . (int) $arData['nid'] . '" AND n.vid="' . (int) $arData['vid'] . '" GROUP BY tid');
        while ($item = $res->Fetch()) {
            $table = Highload::getTable($item);
            $arParam[$table][] = Utility\Highload::searchOldId($table, $item['tid']);
        }
        foreach (array(
    'ACTIONS' => 'Actions',
    'BRANDS' => 'Brands',
    'COLORS' => 'Colors',
    'FRAMES_FORM' => 'FramesForm',
    'LENSES_INDEX' => 'LensesIndex',
    'LENSES_MATERIAL' => 'LensesMaterial',
    'LENSES_INDEX' => 'LensesIndex',
    'LENSES_ASTIGMATISM' => 'LensesAstigmatism',
    'LIGHT_FILTER' => 'Lightfilter',
    'CYL_MIN' => 'CYL_MIN',
    'CYL_MAX' => 'CYL_MAX',
    'ADD_MIN' => 'ADD_MIN',
    'ADD_MAX' => 'ADD_MAX',
    'STORE_STATUS' => 'STORE_STATUS',
        ) as $key => $param) {
            if ($arParam[$param]) {
                $propFields[$key] = $arParam[$param][0]['value'] ?: $arParam[$param][0];
            }
        }
        foreach (array(
    'ACTIONS' => 'Label',
    'SEX' => 'Sex',
//    'COLORS' => 'Colors',
    'FRAMES_TYPE' => 'FramesType',
    'SUN_GLASSES_TYPE' => 'SunGlassesType',
    'SPORT_GLASSES_TYPE' => 'SportGlassesType',
    'ACCESSORY_TYPE' => 'AccessoryType',
    'FRAMES_MATERIAL' => 'FramesMaterial',
    'LENSES_TYPE' => 'LensesType',
    'LENSES_TYPE_FOCUS' => 'LensesTypeFocus',
    'LENSES_TRANSMISSION' => 'LensesTransmission',
        ) as $key => $param) {
            if ($arParam[$param]) {
                $propFields[$key] = $arParam[$param];
            }
        }
        $propFields = array_map('trim', $propFields);

        preExit($propFields);

        Utility\Catalog::update($arItem, $arFields, $propFields);
        Utility\Catalog::saveCatalog($arItem);
        if ($arData['product'] and $arData['product']['sell_price']) {
            Utility\Catalog::savePrice($arItem, $arData['product']['sell_price'], 'RUB');
        }

        switch ($arData['type']) {
            case 'och_linzu':
                self::importLenses($arData, $arItem, $arParam);
                break;

            default:
                break;
        }

        preExit($arItem, $arFields, $propFields);
        Log::success('Добавлены товары', $arItem['NAME']);
    }

    static protected function importLenses($arData, $arItem, $arParam, $propFields) {
        if (isset($arParam['SPH_MIN'])) {
            foreach ($arParam['SPH_MIN'] as $key => $value) {
                $arFields = array(
                    'IBLOCK_ID' => Config::OFFERS_ID,
                    'NAME' => $arItem['NAME'],
                );
                $propFields = array(
                    'CML2_LINK' => $arItem['ID'],
                    'COLOR' => $propFields['COLORS'],
                    'SPH_MIN' => $arParam['SPH_MIN'][$key]['value'] ?: '',
                    'SPH_MAX' => $arParam['SPH_MAX'][$key]['value'] ?: '',
                    'SPH_STEP' => $arParam['SPH_STEP'][$key]['value'] ?: '',
                    'D' => $arParam['D'][$key]['value'] ?: '',
                );
                $propFields['D'] = trim(str_replace(array('мм', 'mm'), '', $propFields['D']));
                $propFields = array_map('trim', $propFields);
                $arFilter = array(
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                );
                foreach ($propFields as $key => $value) {
                    $arFilter['=PROPERTY_' . $key] = $value;
                }
                $arOffers = Utility\Catalog::searchByFilter($arFilter, $arFields, $propFields);
                Utility\Catalog::saveCatalog($arOffers);
                if ($arData['product'] and $arData['product']['sell_price']) {
                    Utility\Catalog::savePrice($arOffers, $arData['product']['sell_price'], 'RUB');
                }
                pre($propFields);
            }
        }
//        preExit($arParam);
    }

}
