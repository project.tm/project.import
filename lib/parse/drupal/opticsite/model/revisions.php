<?php

namespace Project\Import\Parse\Drupal\Opticsite\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class RevisionsTable extends DataManager {

    public static function getConnectionName() {
        return 'drupal';
    }

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'node_revisions';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('nid', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('vid'),
            new Main\Entity\IntegerField('uid'),
            new Main\Entity\StringField('title'),
            new Main\Entity\StringField('body'),
            new Main\Entity\StringField('teaser'),
            new Main\Entity\IntegerField('timestamp'),
            new Main\Entity\IntegerField('format')
        );
    }

}
