<?php

namespace Project\Import\Helper;

use DigitalWand\AdminHelper\Helper;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
abstract class AdminEditHelper extends Helper\AdminEditHelper {

  

    protected function getMenu($showDeleteButton = true) {
        return array();
    }

    protected function saveElement($id = null) {
        return array();
    }

    protected function show404() {

    }

}
