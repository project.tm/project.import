<?php

namespace Project\Import\Traits;

use SimpleXMLElement,
    cFile,
    Project\Import\Parse\Records,
    Project\Log,
    Project\Import\Data,
    Project\Import\Config;

trait Csv {

    static public function parse($sting) {
        preHtml($sting);
        $slash = '++=++';
        $sting = str_replace('""', $slash, $sting);
        $arData = array();
        $start = 0;
        $strlen = strlen($sting);
        $isOpen = false;
        do {
            if ($isOpen) {
                $item = strpos($sting, '";', $start);
                $new = $item + 1;
                $isOpen = false;
            } else {
                $item = strpos($sting, ';', $start);
                $item2 = strpos($sting, ';"', $start);
                if ($item >= $item2 and $item2) {
                    $isOpen = true;
                    $item = $item2;
                    $new = $item + 1;
                } else {
                    $new = $item;
                }
            }
            if ($new == false) {
                $arData[] = trim(str_replace($slash, '"', substr($sting, $start)));
            } else {
                $arData[] = trim(str_replace($slash, '"', substr($sting, $start, $item - $start)));
            }
            if ($start > $strlen) {
                break;
            }
            $start = $new + 1;
        } while ($item !== false);
        return $arData;
    }

    static public function process($page) {
//        $GLOBALS['APPLICATION']->RestartBuffer();
//        if ($page == 1) {
//            Records::clear();
//        }
        $limit = Config::LIMIT;
        $start = ($page - 1) * $limit;
        $end = ($page) * $limit;

        $filename = $_SERVER["DOCUMENT_ROOT"] . cFile::GetPath(Data::get('FILE'));
        if (file_exists($filename)) {
            $filelen = strlen(file_get_contents($filename));
            $key = -1;
//            echo '<h3>Выполнено ' . round(($page - 1) / ceil(count($arData) / $limit) * 100, 2) . '% (' . $start . '/' . count($arData) . ')</h3>';
            echo '<h3>Разобрано ' . ($page - 1) * $limit .' строк</h3>';
            set_time_limit(3600);
            if (($handle = fopen($filename, "r")) !== FALSE) {
//                pre($handle);
                while (($data = fgetcsv($handle, 10000, ";")) !== FALSE) {
                    $key++;
                    if ($key < $start) {
                        continue;
                    }
                    if ($key >= $end) {
                        return true;
                    }
                    if (empty($key) or empty($data)) {
                        continue;
                    }
                    $arData = array_map(function($value) {
                       return iconv('CP1251', "UTF-8", $value);
                    }, $data);
                    static::importProduct($arData);
//                    exit;
                };
                fclose($handle);
            }            
        }
        return false;
    }

}
