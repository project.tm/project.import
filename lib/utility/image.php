<?php

namespace Project\Import\Utility;

use CFile;

class Image {

    static public function upload($file, $domen) {
        $path = $_SERVER["DOCUMENT_ROOT"] . '/upload/tmp/project.import/' . $file;
        if (!file_exists($path)) {
            CheckDirPath($path);
            file_put_contents($path, file_get_contents($domen . $file));
            if (!file_exists($path)) {
                return false;
            }
        }
        return CFile::MakeFileArray($path);
    }

}
