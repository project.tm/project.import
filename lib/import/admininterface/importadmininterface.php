<?php

namespace Project\Import\Import\AdminInterface;

use Bitrix\Main\Localization\Loc,
    DigitalWand\AdminHelper,
    Project\Import\Config;

Loc::loadMessages(__FILE__);

/**
 * Описание интерфейса (табок и полей) админки новостей.
 *
 * {@inheritdoc}
 */
class ImportAdminInterface extends AdminHelper\Helper\AdminInterface {

    /**
     * {@inheritdoc}
     */
    public function fields() {
        return array(
            'MAIN' => array(
                'NAME' => Loc::getMessage('PROJECT_IMPORT_HEADER'),
                'FIELDS' => array(
                    'FILE' => array(
                        'WIDGET' => new AdminHelper\Widget\FileWidget(),
                        'IMAGE' => false,
                        'HEADER' => false
                    ),
                    'FILE_TYPE' => array(
                        'WIDGET' => new AdminHelper\Widget\ComboBoxWidget(),
                        'VARIANTS' => Config::ROUTER
                    )
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function helpers() {
        return array(
            '\Project\Import\Import\AdminInterface\ImportListHelper',
            '\Project\Import\Import\AdminInterface\ImportEditHelper'
        );
    }

}
