<?php

namespace Project\Import\Import\AdminInterface;

use Project\Import\Helper\AdminEditHelper,
    Project\Import\Log,
    Project\Import\Router,
    Project\Import\View;

/**
 * Хелпер описывает интерфейс, выводящий форму редактирования новости.
 *
 * {@inheritdoc}
 */
class ImportEditHelper extends AdminEditHelper {

    protected static $model = '\Project\Import\Import\ImportTable';

    protected function showProlog() {
        Router::process();
    }

    protected function showEpilog() {
        View::timeEnd();
        echo implode('<br>', Log::get());
        parent::showEpilog();
    }

    protected function saveElement($id = null) {
        if (empty($_FILES['FIELDS']['error']['FILE'])) {
            Router::start($this->data);
        }

        return false;
    }

    protected function setElementTitle() {
        $this->setTitle('Импорт');
    }

}
