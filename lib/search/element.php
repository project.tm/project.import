<?php

namespace Project\Import\Search;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class ElementTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('ACTIVE'),
            new Main\Entity\StringField('IBLOCK_ID'),
            new Main\Entity\DatetimeField('ACTIVE_FROM'),
            new Main\Entity\StringField('NAME'),
        );
    }

}
