<?php

namespace Project\Import\Search;

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class PropsTable extends DataManager {

    /**
     * {@inheritdoc}
     */
    public static function getTableName() {
        return 'b_iblock_element_property';
    }

    /**
     * {@inheritdoc}
     */
    public static function getMap() {
        return array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\StringField('IBLOCK_PROPERTY_ID'),
            new Main\Entity\IntegerField('IBLOCK_ELEMENT_ID'),
            new Main\Entity\IntegerField('VALUE'),
        );
    }

}
