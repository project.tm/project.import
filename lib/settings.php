<?php

namespace Project\Import;

use Bitrix\Main\Config\Option,
    Project\Import\Config;

class Settings {

    static public function get($key) {
        return Option::get(Config::MODULE, $key);
    }

}
