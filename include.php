<?php

use Bitrix\Main\Loader;

Loader::includeModule('project.core');
Loader::includeModule('project.redirect');
Loader::includeModule('iblock');
Loader::includeModule('highloadblock');
Loader::includeModule('catalog');
