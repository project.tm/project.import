<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Project\Import\Logs\AdminInterface\LogsListHelper;
use Project\Import\Logs\AdminInterface\LogsEditHelper;
use Project\Import\Import\AdminInterface\ImportEditHelper;

if (!Loader::includeModule('digitalwand.admin_helper') || !Loader::includeModule('project.import')) return;

Loc::loadMessages(__FILE__);

return array(
    array(
        'parent_menu' => 'global_menu_services',
        'sort' => 300,
        'icon' => 'fileman_sticker_icon',
        'page_icon' => 'fileman_sticker_icon',
        'text' => Loc::getMessage('PROJECT_IMPORT_UPLOAD'),
        'url' => ImportEditHelper::getUrl(),
    ),
);